$(function() {
	// media query event handler
	if (matchMedia) {
	  const mq = window.matchMedia("(max-width: 400px)");
	  mq.addListener(WidthChange);
	  WidthChange(mq);
	}

	// media query change
	function WidthChange(mq) {
	  if (mq.matches) {
	    console.log("It is small devices");
	  } else {
	    console.log("It is large devices");
	  }

	}
});