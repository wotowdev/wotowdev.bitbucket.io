$(document).ready(function() {

  $(window).on('load', function(){
    resize();
  });

  $(window).on('resize', function(){
    resize();
  });

  function resize() {
    var windowHeight = $(window).height();
    var windowWidth = $(window).width();
    $('.container-left').css({height:windowHeight});
    $('.container-right').css({height:windowHeight});
    $('.logo-centre').css({height:windowHeight});
  }
});


/*$(window).on('load, resize', function fullViewPort() {
    var viewportWidth = $(window).width();
    var viewportHeight = $(window).height();
    $('.container-left').css({height:windowHeight});
    $('.container-right').css({height:windowHeight});
    $('.logo-centre').css({height:windowHeight});
});*/